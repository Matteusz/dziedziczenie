package vehicles;

public class Engine {
    private int enginePower;
    private String engineType;

    public Engine(int enginePower, String engineType) {
        this.enginePower = enginePower;
        this.engineType = engineType;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }
}
