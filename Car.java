package vehicles;

public class Car extends WheeldVehicle {
    private int doors;


    void pritntInfo() {
        System.out.println("Samochód: silnik " + getEngine().getEngineType() + " Koła: " + getWheels());
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }
}
