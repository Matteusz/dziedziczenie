package vehicles;

public class Motorbike extends WheeldVehicle {
    private String startType;

    public String getStartType() {
        return startType;
    }

    public void setStartType(String startType) {
        this.startType = startType;
    }
}
